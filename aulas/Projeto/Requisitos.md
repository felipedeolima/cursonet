## Requisitos do Projeto MinhaGrana 📊💰

Este projeto visa criar um sistema de gerenciamento de finanças pessoais que seja intuitivo, eficiente e seguro para os usuários. 

# Histórias de usuários ([Coleção](https://docs.google.com/document/d/1-z7MXlJawZcKVb3TsbEKChwxNPKj04x2sEzp86_ijNw/edit))

# Registro de Despesas e Receitas:
- Os usuários devem poder registrar suas despesas e receitas de forma fácil e rápida.
- O sistema deve permitir a inclusão de informações detalhadas, como data, categoria e descrição da transação.
## Categorização de Transações:
- Os usuários devem poder categorizar suas transações para melhor organização.
- Categorias comuns podem incluir alimentação, transporte, moradia, entretenimento, etc.
## Acompanhamento de Saldos:
- O sistema deve exibir o saldo atual dos usuários com base em suas transações registradas.
- Os usuários devem poder visualizar seus saldos de forma clara e concisa.
## Relatórios Financeiros:
- Os usuários devem ter acesso a relatórios detalhados sobre suas finanças.
- Relatórios podem incluir gráficos de despesas mensais, análise de tendências e projeções futuras.
## Conexão com Contas Bancárias via Open Banking:
- Os usuários devem poder vincular suas contas bancárias ao sistema.
- O sistema deve usar APIs de Open Banking para obter automaticamente as movimentações financeiras dos usuários.
## Atualização Automática das Movimentações:
- As transações das contas bancárias vinculadas devem ser atualizadas automaticamente no sistema.
- Isso garante que os usuários tenham dados precisos e atualizados em tempo real.
## Segurança e Privacidade:
- O sistema deve adotar práticas de segurança robustas para proteger as informações financeiras dos usuários
- A autenticação via SSO é essencial.

Sempre envolver os usuários em testes e feedback contínuos para garantir que o sistema atenda às suas necessidades. 📊💰

Minha Grana