﻿using MinhaGrana.Data;
using Microsoft.AspNetCore.Mvc;
using MinhaGrana.Models;
using Microsoft.AspNetCore.Authorization;
using MinhaGrana.Core;
using MinhaGrana.Responses;
using System.Runtime.InteropServices;

namespace MinhaGrana.Controllers;

/// <summary>
/// 
/// </summary>
[Route("[controller]")]
[ApiController]
public class BancoController : ControllerBase
{
    private readonly RelatorioService<Banco> _relatorioService;

    /// <summary>
    /// 
    /// </summary>
    public BancoController()
    {
        _relatorioService = new RelatorioService<Banco>();
    }

    /// <summary>
    /// Lista todos os Bancos
    /// </summary>
    /// <param name="dal"></param>
    /// <returns></returns>
    [Authorize]
    [HttpGet("")]
    public ActionResult Get([FromServices] DAL<Banco> dal)
    {
        if (dal is null) return NotFound();
        return Ok(dal.Listar());
    }

    /// <summary>
    /// Recupera um Banco pelo ID
    /// </summary>
    /// <param name="dal"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize]
    [HttpGet("{id}")]
    public ActionResult RetrieveCompe([FromServices] DAL<Banco> dal, int id)
    {
        var banco = dal.RecuperarPor(b => b.IdBanco == id);
        if (banco is null) return NotFound();
        return Ok(banco);
    }

    /// <summary>
    /// Adiciona um Banco
    /// </summary>
    /// <param name="dal"></param>
    /// <param name="banco"></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpPost("")]
    public ActionResult Add([FromServices] DAL<Banco> dal, [FromBody] Banco banco)
    {
        dal.Adicionar(banco);
        return (Ok(banco));
    }

    /// <summary>
    /// Atualiza um Banco
    /// </summary>
    /// <param name="dal"></param>
    /// <param name="banco"></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpPut("")]
    public ActionResult Update([FromServices] DAL<Banco> dal, [FromBody] Banco banco)
    {
        var bancoAAtualizar = dal.RecuperarPor(b => b.IdBanco == banco.IdBanco);

        if (bancoAAtualizar is null) return NotFound();
        bancoAAtualizar.Nome = banco.Nome;

        dal.Atualizar(bancoAAtualizar);
        return Ok(bancoAAtualizar);
    }

    /// <summary>
    /// Apaga o Banco do ID informado
    /// </summary>
    /// <param name="dal"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete("{id}")]
    public ActionResult Delete([FromServices] DAL<Banco> dal, int id)
    {
        var banco = dal.RecuperarPor(a => a.IdBanco == id);
        if (banco is null) return NotFound();
        dal.Deletar(banco);
        return NoContent();
    }

    /// <summary>
    /// Importa a lista de Bancos a partir do CSV
    /// </summary>
    /// <param name="dal"></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpPost("importar")]
    public ActionResult Import([FromServices] DAL<Banco> dal)
    {
        var enderecoArquivo = "bancos.csv";
        int cont = 0;

        using (var fluxoDeArquivo = new FileStream(enderecoArquivo, FileMode.Open))
        {
            var leitor = new StreamReader(fluxoDeArquivo);
            if (leitor is null) return NotFound();
            while (!leitor.EndOfStream)
            {
                cont++;
                var linha = leitor.ReadLine();
                var banco = ConverterStringParaBanco(linha!);
                dal.Adicionar(banco);
            }
        }
        return Ok($"{cont} registros importados!");
    }

    /// <summary>
    /// Converte a linha para o objeto Banco
    /// </summary>
    /// <param name="linha"></param>
    /// <returns></returns>
    static Banco ConverterStringParaBanco(string linha)
    {
        var campos = linha.Split(';');
        var banco = new Banco();
        banco.IdBanco = int.Parse(campos[0]);
        banco.Nome = campos[1];

        return banco;
    }

    /// <summary>
    /// Gera o relatório de Bancos
    /// </summary>
    /// <param name="dal"></param>
    /// <returns></returns>
    [HttpGet("ObterRelatorioPdf")]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiSucessoResponse))]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> ObterRelatorioPdf([FromServices] DAL<Banco> dal)
    {
        var documento = await _relatorioService.CriarRelatorioAsync(dal);
        string mimeType = "application/octet-stream";

        var fileContentResult = new FileContentResult(documento, mimeType)
        {
            FileDownloadName = $"Relatorio_Banco_{DateTime.Now.ToString("yyyy_MM_dd")}.pdf"
        };

        return fileContentResult;
    }
}