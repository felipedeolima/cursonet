using MinhaGrana.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RouteAttribute = Microsoft.AspNetCore.Components.RouteAttribute;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using MinhaGrana.Data;
using MinhaGrana.Core;

namespace MinhaGrana.Controllers;

/// <summary>
/// 
/// </summary>
[Route("[controller]")]
[ApiController]
public class AuthController :ControllerBase
{
	private readonly UserManager<IdentityUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly IConfiguration _configuration;
    private readonly AuthService _authService;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="userManager"></param>
    /// <param name="roleManager"></param>
    /// <param name="configuration"></param>
    /// <param name="authService"></param>
	public AuthController(
        UserManager<IdentityUser> userManager,
        RoleManager<IdentityRole> roleManager,
        IConfiguration configuration,
        AuthService authService)
    {
        _userManager = userManager;
        _roleManager = roleManager;
        _configuration = configuration;
        _authService = authService;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpPost("login")]
    [AllowAnonymous]
    public async Task<ActionResult> Login([FromBody] Login model)
    {
        var user = await _userManager.FindByEmailAsync(model.Email);
        if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
        {
            var roles = await _userManager.GetRolesAsync(user);
            var role = roles.FirstOrDefault();
            if (role == null) return Unauthorized(new { Status = "Error", Message = "Usuário sem permissão!" });
            var authClaims = new List<Claim>
            {
                new (ClaimTypes.Name, user.UserName),
                new (JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new (ClaimTypes.Role, role)
            };

            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:key"]));
            var token = new JwtSecurityToken(
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );
            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = token.ValidTo
            });
        }
        return Unauthorized(new { Status = "Error", Message = "Autenticação inválida. Favor verificar dados de acesso!" });
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpGet("listarUsuarios")]
    //public ActionResult Get([FromServices] DAL<IdentityUser> dal)
    public ActionResult Get()
    {
        return Ok(_authService.Listar());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    //[Authorize(Roles = "Admin")]
    [HttpPost("registrarAdmin")]
	public async Task<ActionResult> RegisterAdmin([FromBody] Register model)
	{
		var userExists = await _userManager.FindByEmailAsync(model.Email);
		if (userExists != null)
			return StatusCode(
				StatusCodes.Status500InternalServerError,
				 new { Status = "Error", Message = "Usuário já existe!" }
				 );
		IdentityUser user = new()
		{
			Email = model.Email,
			SecurityStamp = Guid.NewGuid().ToString(),
			UserName = model.Email
		};

		var result = await _userManager.CreateAsync(user, model.Password);
		if (!result.Succeeded)
			return StatusCode(
				StatusCodes.Status500InternalServerError,
				new {
					Status = "Error",
					Message = "Falha ao criar o usuário! Favor, checar os detalhes e solicitar novamente."
					});
        if (!await _roleManager.RoleExistsAsync("Admin"))
            await _roleManager.CreateAsync(new IdentityRole("Admin"));
        await _userManager.AddToRoleAsync(user, "Admin");
        return Ok(new { Status = "Success", Message = "Usuário Administrador criado com sucesso."});
	}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpPost("registrarUser")]
    public async Task<ActionResult> RegisterUser([FromBody] Register model)
    {
        var userExists = await _userManager.FindByEmailAsync(model.Email);
        if (userExists != null)
            return StatusCode(
                StatusCodes.Status500InternalServerError,
                 new { Status = "Error", Message = "Usuário já existe!" }
                 );
        IdentityUser user = new()
        {
            Email = model.Email,
            SecurityStamp = Guid.NewGuid().ToString(),
            UserName = model.Email
        };

        var result = await _userManager.CreateAsync(user, model.Password);
        if (!result.Succeeded)
            return StatusCode(
                StatusCodes.Status500InternalServerError,
                new
                {
                    Status = "Error",
                    Message = "Falha ao criao o usuário! Favor, checar os detalhes e solicitar novamente."
                });
        if (!await _roleManager.RoleExistsAsync("User"))
            await _roleManager.CreateAsync(new IdentityRole("User"));
        await _userManager.AddToRoleAsync(user, "User");
        return Ok(new { Status = "Success", Message = "Usuário criado com sucesso." });
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dal"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete("removerUsuario")]
    public ActionResult Delete([FromServices] DAL<IdentityUser> dal, string id)
    {
        var user = dal.RecuperarPor(u => u.Id.Equals(id));
        if (user is null) return NotFound();
        dal.Deletar(user);
        return NoContent();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    //[Authorize(Roles = "Admin,User")]
    [HttpGet("gerarChave")]
    public ActionResult Key()
    {
        var key = new byte[32];
        using (var rng = RandomNumberGenerator.Create())
        {
            rng.GetBytes(key);
        }

        return Ok(new
        {
            Status = "Success",
            Message = $"Comando para rodar no CLI: dotnet user-secrets set 'Jwt:Key' '{Convert.ToBase64String(key)}'"
        });
    }

    
}


