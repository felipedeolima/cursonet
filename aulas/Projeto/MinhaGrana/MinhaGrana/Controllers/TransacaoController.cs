﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MinhaGrana.Core;
using MinhaGrana.Data;
using MinhaGrana.Data.DTO;
using MinhaGrana.DTO;
using MinhaGrana.Models;
using MinhaGrana.Responses;

namespace MinhaGrana.Controllers;

/// <summary>
/// Controlador da Transação 
/// </summary>
[Route("[controller]")]
[ApiController]
public class TransacaoController: ControllerBase
{
    private TransacaoService _transacaoService;

    private readonly RelatorioService<Transacao> _relatorioService;

    /// <summary>
    /// Controlador da Transação, passando o serviço para acessar a camada de regras
    /// </summary>
    /// <param name="service">Acessa as regras de negócio e faz os controles do DTO</param>
    public TransacaoController(TransacaoService service)
    {
        _transacaoService = service;
        _relatorioService = new RelatorioService<Transacao>();
    }

    /// <summary>
    /// Lista todas as transações dos usuários
    /// </summary>
    /// <returns>DTO das transações</returns>
    [Authorize(Roles = "Admin")]
    [HttpGet("listar")]
    public ActionResult Get()
    {
        return Ok(_transacaoService.Listar());
    }

    /// <summary>
    /// Listar por conta bancária todas as transações do usuário
    /// </summary>
    /// <returns>DTO das transações</returns>
    [Authorize]
    [HttpGet("listarPorConta/{conta}")]
    public ActionResult GetByConta(string conta)
    {
        return Ok(_transacaoService.Listar(conta));
    }

    /// <summary>
    /// Recupera todas as transações pelo nome da transação
    /// </summary>
    /// <param name="nomeTransacao">Nome da transação de forma genérica</param>
    /// <returns>Todas as transações</returns>
    /// <response code="200">Caso recuperação seja feita com sucesso</response>
    /// <response code="401">Não autenticado</response>
    /// <response code="404">Registro não encontrado</response>
    [Authorize]
    [HttpGet("recuperar/{nomeTransacao}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public ActionResult Retrieve(string nomeTransacao)
    {
        var transacao = _transacaoService.ListarPorNome(nomeTransacao);
        if (transacao.Count() == 0) return NotFound("Não encontrado");
        return Ok(transacao);
    }

    /// <summary>
    /// Insere a transação de acordo com o DTO repassado
    /// </summary>
    /// <param name="dal"></param>
    /// <param name="dalConta"></param>
    /// <param name="dalCategoria"></param>
    /// <param name="transacaoDTO"></param>
    /// <returns></returns>
    [Authorize]
    [HttpPost("inserir")]
    public ActionResult Add([FromServices] DAL<Transacao> dal,
                            [FromServices] DAL<ContaBancaria> dalConta,
                            [FromServices] DAL<Categoria> dalCategoria, [FromBody] TransacaoCreateDTO transacaoDTO)
    {
        //var retorno = _transacaoService.Adicionar(t);
        Transacao transacao = new();

        transacao.Nome = transacaoDTO.Nome;
        transacao.Valor = transacaoDTO.Valor;
        transacao.Tipo = transacaoDTO.Tipo;
        transacao.Data = transacaoDTO.Data;
        transacao.ContaBancaria = dalConta.RecuperarPor(b => b.IdContaBancaria == transacaoDTO.ContaBancariaId);
        transacao.Categoria = dalCategoria.RecuperarPor(u => u.IdCategoria == transacaoDTO.CategoriaId);


        if (transacao.ContaBancaria is not null)
        {
            //Crédito
            if (transacaoDTO.Tipo == 'C')
            {
                transacao.ContaBancaria.ValorDisponivel += transacao.Valor;
            }
            else //Débito
            {
                transacao.ContaBancaria.ValorDisponivel -= transacao.Valor;
            }
            dal.Adicionar(transacao);
            return Ok($"Transacao concluída com sucesso! Novo saldo do cliente: R${transacao.ContaBancaria.ValorDisponivel}");
        }
        return Ok($"Não existe conta bancária vinculada a este identificador!");        
    }

    /// <summary>
    /// Atualizar transação de uma conta bancária
    /// </summary>
    /// <param name="dal"></param>
    /// <param name="dalConta"></param>
    /// <param name="dalCategoria"></param>
    /// <param name="transacaoDTO"></param>
    /// <returns>Transação atualizada ou não encontrada transação/conta bancária</returns>
    [Authorize]
    [HttpPut("atualizar")]
    public ActionResult UpdateTransacao([FromServices] DAL<Transacao> dal,
                            [FromServices] DAL<ContaBancaria> dalConta,
                            [FromServices] DAL<Categoria> dalCategoria,
                            [FromBody] TransacaoPutDTO transacaoDTO)
    {
        var transacao = dal.RecuperarPor(t => t.IdTransacao == transacaoDTO.Id);

        if (transacao is null) return NotFound();

        //Atualizando os dados
        transacao.ContaBancaria = dalConta.RecuperarPor(b => b.IdContaBancaria == transacaoDTO.ContaBancariaId);
        transacao.Categoria = dalCategoria.RecuperarPor(u => u.IdCategoria == transacaoDTO.CategoriaId);

        if (transacao.ContaBancaria is not null)
        {
            if (transacaoDTO.Tipo == 'C')
            {
                transacao.ContaBancaria.ValorDisponivel -= transacao.Valor;
            }
            else
            {
                transacao.ContaBancaria.ValorDisponivel += transacao.Valor;
            }
        }

        transacao.IdTransacao = transacaoDTO.Id;
        transacao.Nome = transacaoDTO.Nome;
        transacao.Valor = transacaoDTO.Valor;
        transacao.Tipo = transacaoDTO.Tipo;
        transacao.Data = transacaoDTO.Data;

        if (transacao.ContaBancaria is not null)
        {
            //Crédito
            if (transacao.Tipo == 'C')
            {
                transacao.ContaBancaria.ValorDisponivel += transacaoDTO.Valor;
            }
            else //Débito
            {
                transacao.ContaBancaria.ValorDisponivel -= transacaoDTO.Valor;
            }
            dal.Atualizar(transacao);
            return Ok($"Transação atualizada com sucesso na conta {transacao.ContaBancaria.Conta}. Valor Disponível: {transacao.ContaBancaria.ValorDisponivel}");
        }
        else
        {
            return Ok($"Não existe conta bancária vinculada a este identificador!");
        }
    }

    /// <summary>
    /// Exclusão de uma transação
    /// </summary>
    /// <param name="id">Identificador da transação</param>
    /// <returns>Transação excluída</returns>
    /// <response code="200">Caso recuperação seja feita com sucesso</response>
    /// <response code="401">Não autenticado</response>
    /// <response code="404">Registro não encontrado</response>
    [Authorize]
    [HttpDelete("remover")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public ActionResult DeleteTransacao([FromBody] int id)
    {
        var transacao = _transacaoService.Excluir(id);
        if (!transacao) return NotFound();
        return NoContent();
    }

    /// <summary>
    /// Obter relatório em PDF com a conta e as transações
    /// </summary>
    /// <param name="conta">Conta que será consultada</param>
    /// <returns>Arquivo em PDF para download</returns>
    [HttpGet("ObterRelatorioPdf/{conta}")]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiSucessoResponse))]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> ObterRelatorioPdf(string conta)
    {
        var documento = await _transacaoService.CriarRelatorioAsyncTransacao(conta);
        string mimeType = "application/octet-stream";

        var fileContentResult = new FileContentResult(documento, mimeType)
        {
            FileDownloadName = $"Relatorio_Transacao_{DateTime.Now.ToString("yyyy_MM_dd")}.pdf"
        };

        return fileContentResult;
    }
}
