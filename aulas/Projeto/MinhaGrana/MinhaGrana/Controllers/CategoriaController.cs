﻿using MinhaGrana.Data;
using Microsoft.AspNetCore.Mvc;
using MinhaGrana.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using MinhaGrana.Responses;
using MinhaGrana.Core;
using MinhaGrana.Data.DTO;
using static iText.StyledXmlParser.Jsoup.Select.Evaluator;

namespace MinhaGrana.Controllers;
/// <summary>
/// Categorias de Transações Bancárias
/// </summary>
[Route("[controller]")]
[ApiController]
public class CategoriaController : ControllerBase
{
    private readonly UserManager<IdentityUser> _userManager;
    private readonly RelatorioService<Categoria> _relatorioService;
    private CategoriaService _categoriaService;

    /// <summary>
    /// Construtor
    /// </summary>
    public CategoriaController(CategoriaService service, UserManager<IdentityUser> userManager)
    {
        _relatorioService = new RelatorioService<Categoria>();
        _categoriaService = service;
        _userManager = userManager;
    }

    /// <summary>
    /// Listar Categorias por usuário logado
    /// </summary>
    /// <returns></returns>
    [Authorize]
    [HttpGet("listar")]
    public ActionResult Get()
    {
        if (this.User.Identity is null) return NotFound();
        string? user = this.User.Identity.Name;
        if (user == null) return NotFound();
        var listar = _categoriaService.Listar(user);
        return Ok(listar);
    }

    /// <summary>
    /// Listar todas Categorias
    /// </summary>
    /// <returns></returns>
    [Authorize(Roles ="Admin")]
    [HttpGet("listarTodos")]
    public ActionResult GetAll()
    {
        var listar = _categoriaService.Listar();
        return Ok(listar);
    }

    /// <summary>
    /// Inserir categoria - sempre adiciona o usuário logado como "dono" da categoria
    /// </summary>
    /// <param name="categoriaDTO"></param>
    /// <returns></returns>
    [Authorize]
    [HttpPost("inserir")]
    public ActionResult Add([FromBody] CategoriaDTO categoriaDTO)
    {
        if (this.User.Identity is null) return NotFound();
        string? user = this.User.Identity.Name;
        if (user == null) return NotFound();

        var identityUser = _userManager.FindByNameAsync(user);

        _categoriaService.Adicionar(categoriaDTO, identityUser.Result);
        
        return (Ok());
    }

    /// <summary>
    /// Inserir categoria - Para todos os usuários
    /// </summary>
    /// <param name="dal"></param>
    /// <param name="categoriaDTO"></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpPost("inserir_como_admin")]
    public ActionResult AddCategoriaAsAdmin([FromServices] DAL<Categoria> dal, [FromBody] CategoriaDTO categoriaDTO)
    {
        Categoria categoria = new();
        categoria.Nome = categoriaDTO.Nome;
        dal.Adicionar(categoria);
        return (Ok(categoria));
    }

    /// <summary>
    /// Atualiza uma categoria - apenas do usuário logado
    /// </summary>
    /// <param name="categoriaDTO"></param>
    /// <returns></returns>
    [Authorize]
    [HttpPut("atualizar")]
    public ActionResult UpdateCategoria([FromBody] CategoriaDTO categoriaDTO)
    {
        if (this.User.Identity is null) return NotFound();
        string? user = this.User.Identity.Name;
        if (user == null) return NotFound();
        var identityUser = _userManager.FindByNameAsync(user);

        bool resultado = _categoriaService.Atualizar(categoriaDTO, identityUser.Result);
        if (resultado) return (Ok());
        return NotFound();
    }

    /// <summary>
    /// Atualiza uma categoria - de qualquer usuário
    /// </summary>
    /// <param name="categoriaDTO"></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpPut("atualizar_como_admin")]
    public ActionResult UpdateCategoriaAsAdmin([FromBody] CategoriaDTO categoriaDTO)
    {
        bool resultado = _categoriaService.AtualizarComoAdmin(categoriaDTO);
        if (resultado) return (Ok());
        return NotFound();
    }

    /// <summary>
    /// Deleta uma categoria - Apenas do usuário
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize]
    [HttpDelete("{id}")]
    public ActionResult Delete(int id)
    {
        if (this.User.Identity is null) return NotFound();
        string? user = this.User.Identity.Name;
        if (user == null) return NotFound();

        var identityUser = _userManager.FindByNameAsync(user);

        bool resultado = _categoriaService.Deletar(id, identityUser.Result);
        if (resultado) return (Ok());
        return NotFound();
    }

    /// <summary>
    /// Deletar qualquer Categoria - apenas Admin
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete("deletar_como_admin/{id}")]
    public ActionResult DeleteAsAdmin(int id)
    {
        bool resultado = _categoriaService.DeletarComoAdmin(id);
        if (resultado) return (Ok());
        return NotFound();
    }

    /// <summary>
    /// Gera o relatório de Categorias
    /// </summary>
    /// <param name="dal"></param>
    /// <returns></returns>
    [HttpGet("ObterRelatorioPdf")]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiSucessoResponse))]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> ObterRelatorioPdf([FromServices] DAL<Categoria> dal)
    {
        var documento = await _relatorioService.CriarRelatorioAsync(dal);
        string mimeType = "application/octet-stream";

        var fileContentResult = new FileContentResult(documento, mimeType)
        {
            FileDownloadName = $"Relatorio_{this.GetType().Name.Replace("Controller", "")}_{DateTime.Now.ToString("yyyy_MM_dd")}.pdf"
        };

        return fileContentResult;
    }
}