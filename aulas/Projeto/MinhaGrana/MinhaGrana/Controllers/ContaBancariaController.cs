﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MinhaGrana.Core;
using MinhaGrana.Data;
using MinhaGrana.Data.DTO;
using MinhaGrana.Models;

namespace MinhaGrana.Controllers;
/// <summary>
/// ContaBancaria
/// </summary>
[Route("[controller]")]
[ApiController]
public class ContaBancariaController: ControllerBase
{
    private ContaBancariaService _contaService;

    /// <summary>
    /// Serviço da conta bancária
    /// </summary>
    /// <param name="service"></param>
    public ContaBancariaController(ContaBancariaService service)
    {
        _contaService = service;
    }

    /// <summary>
    /// Listagem total das contas bancárias
    /// </summary>
    /// <returns></returns>
    [Authorize(Roles = "Admin")]
    [HttpGet("")]
    public ActionResult Get()
    {
        return Ok(_contaService.Listar());
    }

    /// <summary>
    /// Listagem total das contas bancárias do usuário
    /// </summary>
    /// <returns></returns>
    [Authorize]
    [HttpGet("listarPorUsuario")]
    public ActionResult GetByUser()
    {
        if (this.User.Identity is null) return NotFound();
        string? user = this.User.Identity.Name;
        if (user == null) return NotFound();
        var listar = _contaService.Listar(user);
        return Ok(listar);
    }

    /// <summary>
    ///  Recupera a conta bancária passando o ID da conta
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize]
    [HttpGet("{id}")]
    public ActionResult RecuperarPorId(int id)
    {
        var contabancaria = _contaService.RecuperarPorId(id);
        if (contabancaria is null) return NotFound();
        return Ok(contabancaria);
    }


    /// <summary>
    /// Inserir uma nova conta bancária
    /// </summary>
    /// <param name="dal"></param>
    /// <param name="dalBanco"></param>
    /// <param name="dalUser"></param>
    /// <param name="conta"></param>
    /// <returns>Nova conta bancária</returns>
    [Authorize]
    [HttpPost("")]
    public ActionResult Add([FromServices] DAL<ContaBancaria> dal, 
                            [FromServices] DAL<Banco> dalBanco, 
                            [FromServices] DAL<IdentityUser> dalUser, 
                            [FromBody] ContaBancariaDTO conta)
    {
        ContaBancaria contaBancaria = new();
        contaBancaria.Conta = conta.Conta;
        contaBancaria.ValorDisponivel = conta.ValorDisponivel;
        contaBancaria.OpenBanking = conta.OpenBanking;
        contaBancaria.Limite = conta.Limite;
        if (conta.NomeBanco is null || conta.NomeUsuario is null) return NotFound();
        contaBancaria.Banco = dalBanco.RecuperarPor(b => b.Nome.ToUpper().Equals(conta.NomeBanco.ToUpper()));
        contaBancaria.Usuario = dalUser.RecuperarPor(u => u.Email.ToUpper().Equals(conta.NomeUsuario.ToUpper()));

        dal.Adicionar(contaBancaria);
        return (Ok($"Conta {conta.Conta} adicionada com sucesso para o usuário {conta.NomeUsuario} no banco {conta.NomeBanco}! " +
            $"\nLimite Disponível: R${conta.Limite}" +
            $"\nSaldo na conta: R${conta.ValorDisponivel}"));
    }

    /// <summary>
    /// Atualiza dados da conta bancária
    /// </summary>
    /// <param name="dal"></param>
    /// <param name="dalBanco"></param>
    /// <param name="dalUser"></param>
    /// <param name="contaBancaria"></param>
    /// <returns></returns>
    [Authorize]
    [HttpPut("")]
    public ActionResult UpdateContaBancaria([FromServices] DAL<ContaBancaria> dal,
                                            [FromServices] DAL<Banco> dalBanco,
                                            [FromServices] DAL<IdentityUser> dalUser,
                                            [FromBody] AtualizarContaBancariaDTO contaBancaria)
    {
        var contaBancariaAAtualizar = dal.RecuperarPor(b => b.IdContaBancaria == contaBancaria.IdContaBancaria);

        if (contaBancariaAAtualizar is null) return NotFound();
        contaBancariaAAtualizar.Conta = contaBancaria.Conta;
        contaBancariaAAtualizar.ValorDisponivel = contaBancaria.ValorDisponivel;
        contaBancariaAAtualizar.OpenBanking = contaBancaria.OpenBanking;
        contaBancariaAAtualizar.Limite = contaBancaria.Limite;
        contaBancariaAAtualizar.Banco = dalBanco.RecuperarPor(b => b.Nome.ToUpper().Equals(contaBancaria.NomeBanco!.ToUpper()));
        contaBancariaAAtualizar.Usuario = dalUser.RecuperarPor(u => u.Email.ToUpper().Equals(contaBancaria.NomeUsuario!.ToUpper()));
        dal.Atualizar(contaBancariaAAtualizar);
        return Ok($"Conta {contaBancaria.Conta} atualizada com sucesso!");
    }

    /// <summary>
    /// Remover uma conta bancária pelo ID da conta
    /// </summary>
    /// <param name="dal"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize]
    [HttpDelete("{id}")]
    public ActionResult DeleteContaBancaria([FromServices] DAL<ContaBancaria> dal, int id)
    {
        var contaBancaria = dal.RecuperarPor(a => a.IdContaBancaria == id);
        if (contaBancaria is null) return NotFound();
        dal.Deletar(contaBancaria);
        return NoContent();
    }
}
