﻿using System.Text.Json.Serialization;

namespace MinhaGrana.Responses;

/// <summary>
/// 
/// </summary>
public abstract class ApiResponse
{
    /// <summary>
    /// 
    /// </summary>
    [JsonPropertyOrder(-2)]
    public int StatusCode { get; set; }
    /// <summary>
    /// 
    /// </summary>
    [JsonPropertyOrder(-1)]
    public string? IdResposta { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public ApiResponse() => IdResposta = Guid.NewGuid().ToString();
}