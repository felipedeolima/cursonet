﻿namespace MinhaGrana.Responses
{
    /// <summary>
    /// 
    /// </summary>
    public class ApiSucessoResponse : ApiResponse
    {
        /// <summary>
        /// 
        /// </summary>
        public string? Mensagem { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public object? Retorno { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ApiSucessoResponse() : base() => StatusCode = StatusCodes.Status200OK;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ApiSucessoResponse(string mensagem) : this() => Mensagem = mensagem;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="retorno"></param>
        public ApiSucessoResponse(object retorno) : this() => Retorno = retorno;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="retorno"></param>
        public ApiSucessoResponse(string mensagem, object retorno) : this()
        {
            Mensagem = mensagem;
            Retorno = retorno;
        }
    }
}