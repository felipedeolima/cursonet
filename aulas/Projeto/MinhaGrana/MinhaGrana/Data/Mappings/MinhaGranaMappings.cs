﻿using MinhaGrana.Data.DTO;
using MinhaGrana.Models;
using AutoMapper;

namespace MinhaGrana.Data.Mappings;

/// <summary>
/// 
/// </summary>
public class MinhaGranaMappings: Profile
{
    /// <summary>
    /// 
    /// </summary>
    public MinhaGranaMappings()
    {
        CreateMap<ContaBancariaDAO, ContaBancariaDTO>();
        CreateMap<TransacaoDTO, Transacao>();
        //CreateMap<UsuarioDAO, UsuarioDTO>();
    }
}
