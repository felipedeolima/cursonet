﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MinhaGrana.Data.DTO;
using MinhaGrana.Models;
using static iText.StyledXmlParser.Jsoup.Select.Evaluator;

namespace MinhaGrana.Data;
/// <summary>
/// Camada de acesso a dados da transação
/// </summary>
public class TransacaoDAO
{

    private readonly ApplicationDbContext _context;
    private readonly IMapper _mapper;

    /// <summary>
    /// Construtor da transação, contendo o context EF Core e mapeamento
    /// </summary>
    /// <param name="context"></param>
    /// <param name="mapper"></param>
    public TransacaoDAO(ApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// Recuperação da conta bancária
    /// </summary>
    /// <returns>ContaBancariaDTO com os dados da transação</returns>
    public ContaBancariaDTO RecuperarContaBancaria(string conta)
    {
        return _context.ContasBancarias
            .Include(p => p.Banco)
            .Include(p => p.Usuario)
            .Where(u => u.Conta!.Equals(conta))
            .Select(x => new ContaBancariaDTO
            {
                Conta = x.Conta,
                ValorDisponivel = x.ValorDisponivel,
                Limite = x.Limite,
                NomeUsuario = x.Usuario == null ? "" : x.Usuario.UserName,
                NomeBanco = x.Banco == null ? "" : x.Banco.Nome
            })
            .First();
    }


    /// <summary>
    /// Listagem de todas as transações 
    /// </summary>
    /// <returns>TransacaoDTO com os dados em nomes, sem identificadores</returns>
    public IEnumerable<TransacaoDTO> Listar()
    {
        return _context.Transacoes
            .Include(p => p.Categoria)
            .Include(p => p.ContaBancaria)
            .Select(x => new TransacaoDTO
            {
                Nome = x.Nome,
                Valor = x.Valor,
                Tipo = x.Tipo,
                NomeCategoria = x.Categoria == null ? "" : x.Categoria.Nome,
                ContaBancaria = x.ContaBancaria == null ? "" : x.ContaBancaria.Conta,
                Data = x.Data,
            })
            .ToList();
    }

    /// <summary>
    /// Listagem por conta bancária
    /// </summary>
    /// <returns>TransacaoDTO contendo todas as transações</returns>
    public IEnumerable<TransacaoDTO> ListarPorConta(string conta)
    {
        return _context.Transacoes
            .Include(p => p.Categoria)
            .Include(p => p.ContaBancaria)
            .Where(u => u.ContaBancaria!.Conta.Equals(conta))
            .Select(x => new TransacaoDTO
            {
                Nome = x.Nome,
                Valor = x.Valor,
                Tipo = x.Tipo,
                NomeCategoria = x.Categoria == null? "": x.Categoria.Nome,
                ContaBancaria = x.ContaBancaria == null ? "" : x.ContaBancaria.Conta,
                Data = x.Data,
            })
            .ToList();
    }

    /// <summary>
    /// Listagem por nome da transações
    /// </summary>
    /// <param name="nomeTransacao">Nome da transação</param>
    /// <returns>Todas as transações com aquele nome</returns>
    public IEnumerable<TransacaoDTO> ListarPorNome(string nomeTransacao)
    {
        return _context.Transacoes
            .Include(p => p.Categoria)
            .Include(p => p.ContaBancaria)
            .Where(u => u.Nome.Contains(nomeTransacao))
            .Select(x => new TransacaoDTO
            {
                Nome = x.Nome,
                Valor = x.Valor,
                Tipo = x.Tipo,
                NomeCategoria = x.Categoria == null ? "" : x.Categoria.Nome,
                ContaBancaria = x.ContaBancaria == null ? "" : x.ContaBancaria.Conta,
                Data = x.Data,
            })
            .ToList();
    }

    /// <summary>
    /// Listagem por nome único de transação
    /// </summary>
    /// <param name="nomeTransacao">Nome da transação</param>
    /// <returns></returns>
    public Transacao? ListarPorNomeUnico(string nomeTransacao)
    {
        //if (nomeTransacao == null) return new Transacao();
        var listagem = _context.Transacoes
            .Include(p => p.Categoria)
            .Include(p => p.ContaBancaria)
            .Where(u => u.Nome.Equals(nomeTransacao))
            .FirstOrDefault();

        if (listagem == null) return null;
        return listagem;
    }

    /// <summary>
    /// Listagem por Id
    /// </summary>
    /// <param name="id">Identificador da transação</param>
    /// <returns>Transação única</returns>
    public Transacao? ListarPorId(int id)
    {
        var listagem = _context.Transacoes
            .Include(p => p.Categoria)
            .Include(p => p.ContaBancaria)
            .Where(u => u.IdTransacao.Equals(id))
            .FirstOrDefault();

        if (listagem == null) return null;
        return listagem;
    }

    /// <summary>
    /// Adicionar objeto
    /// </summary>
    /// <param name="objeto">Transação a ser adicionada</param>
    public void Adicionar(Transacao objeto)
    {
        _context.Set<Transacao>().Add(objeto);

        _context.SaveChanges();
    }

    /// <summary>
    /// Atualização da transação
    /// </summary>
    /// <param name="transacao">Transação a ser atualizada</param>
    internal void Atualizar(Transacao transacao)
    {
        _context.Set<Transacao>().Update(transacao);
        _context.SaveChanges();
    }


    /// <summary>
    /// Exclusão da transação
    /// </summary>
    /// <param name="transacao">Transação a ser excluída</param>
    public void Deletar(Transacao transacao)
    {
        _context.Set<Transacao>().Remove(transacao);
        _context.SaveChanges();
    }
}

