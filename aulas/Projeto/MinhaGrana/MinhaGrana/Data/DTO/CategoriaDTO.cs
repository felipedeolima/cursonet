﻿namespace MinhaGrana.Data.DTO
{
    /// <summary>
    /// 
    /// </summary>
    public class CategoriaDTO
    {
        /// <summary>
        /// Id da Categoria
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Nome
        /// </summary>
        public string Nome { get; set; } = string.Empty;

        /// <summary>
        /// Id do Usuário
        /// </summary>
        public string IdUsuario { get; set; } = string.Empty;
    }
}
