﻿namespace MinhaGrana.DTO
{
    /// <summary>
    /// 
    /// </summary>
    public class TransacaoPutDTO
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Nome { get; set; } = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public decimal Valor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public char Tipo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Data { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CategoriaId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ContaBancariaId { get; set; }

    }
}
