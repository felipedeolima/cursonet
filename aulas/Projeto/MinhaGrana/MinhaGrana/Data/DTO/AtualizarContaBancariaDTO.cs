﻿namespace MinhaGrana.Data.DTO;

/// <summary>
/// 
/// </summary>
public class AtualizarContaBancariaDTO
{
     /// <summary>
     /// 
     /// </summary>
     public int IdContaBancaria { get; set; }
     /// <summary>
     /// 
     /// </summary>
     public string Conta { get; set; } = string.Empty;
     /// <summary>
     /// 
     /// </summary>
     public decimal ValorDisponivel { get; set; }
     /// <summary>
     /// 
     /// </summary>
     public decimal Limite { get; set; }
     /// <summary>
     /// 
     /// </summary>
     public bool OpenBanking { get; set; }

     /// <summary>
     /// 
     /// </summary>
     public string? NomeUsuario { get; set; }

     /// <summary>
     /// 
     /// </summary>
     public string? NomeBanco { get; set; }
}

