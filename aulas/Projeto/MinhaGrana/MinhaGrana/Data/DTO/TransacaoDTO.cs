﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MinhaGrana.Data.DTO
{
    /// <summary>
    /// 
    /// </summary>
    public class TransacaoDTO
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nome { get; set; } = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public decimal Valor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public char Tipo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Data { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string? NomeCategoria { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int IdCategoria { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string? ContaBancaria { get; set; }

    }
}
