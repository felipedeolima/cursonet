﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MinhaGrana.Data.DTO;
using MinhaGrana.Models;

namespace MinhaGrana.Data;
/// <summary>
/// 
/// </summary>
public class CategoriaDAO
{

    private readonly ApplicationDbContext _context;
    private readonly IMapper _mapper;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="mapper"></param>
    public CategoriaDAO(ApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IEnumerable<CategoriaDTO> Listar()
    {
        return _context.Categorias //_mapper.Map<List<ContaBancariaDTO>>(
            .Include(p => p.Usuario)
            .Select(x => new CategoriaDTO
            {
                Id = x.IdCategoria,
                IdUsuario = x.Usuario!.Id,
                Nome = x.Nome,
            })
            .ToList();
    }

    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="objeto"></param>
    public void Adicionar(Categoria objeto)
    {
        _context.Set<Categoria>().Add(objeto);

        _context.SaveChanges();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="objeto"></param>
    public void Atualizar(Categoria objeto)
    {
        _context.Set<Categoria>().Update(objeto);
        _context.SaveChanges();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="objeto"></param>
    public void Deletar(Categoria objeto)
    {
        _context.Set<Categoria>().Remove(objeto);
        _context.SaveChanges();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="condicao"></param>
    /// <returns></returns>
    public CategoriaDTO RecuperarPor(Func<CategoriaDAO, bool> condicao)
    {
        return _context.Categorias 
        .Include(p => p.Usuario)
        .Select(x => new CategoriaDTO
        {
            Id = x.IdCategoria,
            IdUsuario = x.Usuario!.Id,
            Nome = x.Nome,
        })
        .First();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="nomeCategoria"></param>
    /// <returns></returns>
    public Categoria RecuperarPorNomeCategoria(string? nomeCategoria)
    {
        return _context.Categorias
        .Include(p => p.Usuario)
        .Where(p => p.Nome.Equals(nomeCategoria))
        .First();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    public IEnumerable<CategoriaDTO> ListarCategoriaPorUsuario(string user)
    {
        var id = _context.Users
            .Where(x => x.UserName == user)
            .Select(x => x.Id)
            .FirstOrDefault();

        if (id == null) return Enumerable.Empty<CategoriaDTO>();

        return _context.Categorias
            .Include(u => u.Usuario)
            .Where(u => (u.Usuario == null ? "" : u.Usuario.Id) == id.ToString() || u.Usuario == null)
            .Select(x => new CategoriaDTO
            {
                Id = x.IdCategoria,
                Nome = x.Nome,
                IdUsuario = x.Usuario == null ? "" : x.Usuario.Id
            })
            .ToList();
    }

    /// <param name="id"></param>
    /// <returns></returns>
    public Categoria RecuperarPorId(int id)
    {
        return _context.Categorias
        .Include(p => p.Usuario)
        .Where(p => p.IdCategoria == id)
        .First();
    }
}

