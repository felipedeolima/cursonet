﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MinhaGrana.Data.DTO;
using MinhaGrana.Models;

namespace MinhaGrana.Data;
/// <summary>
/// 
/// </summary>
public class ContaBancariaDAO
{

    private readonly ApplicationDbContext _context;
    private readonly IMapper _mapper;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="mapper"></param>
    public ContaBancariaDAO(ApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IEnumerable<ContaBancariaDTO> Listar()
    {
        return _context.ContasBancarias //_mapper.Map<List<ContaBancariaDTO>>(
            .Include(p => p.Banco)
            .Include(p => p.Usuario)
            .Select(x => new ContaBancariaDTO
            {
                IdContaBancaria = x.IdContaBancaria,
                Conta = x.Conta,
                ValorDisponivel = x.ValorDisponivel,
                Limite = x.Limite,
                OpenBanking = x.OpenBanking,
                NomeUsuario = x.Usuario == null ? "" : x.Usuario.UserName,
                NomeBanco = x.Banco == null ? "" : x.Banco.Nome
            })
            .ToList();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    public IEnumerable<ContaBancariaDTO> ListarContaPorUsuario(string user)
    {
        var id = _context.Users
            .Where(x => x.UserName == user)
            .Select(x => x.Id)
            .FirstOrDefault();

        if (id == null) return Enumerable.Empty<ContaBancariaDTO>();

        return _context.ContasBancarias
            .Include(b => b.Banco)
            .Include(u => u.Usuario)
            .Where(u => (u.Usuario == null ? "" : u.Usuario.Id) == id.ToString())
            .Select(x => new ContaBancariaDTO
            {
                IdContaBancaria = x.IdContaBancaria,
                Conta = x.Conta,
                ValorDisponivel = x.ValorDisponivel,
                Limite = x.Limite,
                OpenBanking = x.OpenBanking,
                NomeUsuario = x.Usuario == null ? "" : x.Usuario.UserName,
                NomeBanco = x.Banco == null ? "" : x.Banco.Nome
            })
            .ToList();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="objeto"></param>
    public void Adicionar(ContaBancariaDAO objeto)
    {
        _context.Set<ContaBancariaDAO>().Add(objeto);

        _context.SaveChanges();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="objeto"></param>
    public void Atualizar(ContaBancaria objeto)
    {
        _context.Set<ContaBancaria>().Update(objeto);
        _context.SaveChanges();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="objeto"></param>
    public void Deletar(ContaBancariaDAO objeto)
    {
        _context.Set<ContaBancariaDAO>().Remove(objeto);
        _context.SaveChanges();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="condicao"></param>
    /// <returns></returns>
    public ContaBancariaDAO? RecuperarPor(Func<ContaBancariaDAO, bool> condicao)
    {

        return _context.Set<ContaBancariaDAO>().FirstOrDefault(condicao);

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="conta"></param>
    /// <returns></returns>
    public ContaBancaria? RecuperarPorConta(String conta)
    {
        return _context.ContasBancarias
            .Include(b => b.Banco)
            .Include(u => u.Usuario)
            .Where(u => u.Conta == conta)
            .Select(x => new ContaBancaria
            {
                IdContaBancaria = x.IdContaBancaria,
                Conta = x.Conta,
                ValorDisponivel = x.ValorDisponivel,
                Limite = x.Limite,
                OpenBanking = x.OpenBanking,
                Usuario = x.Usuario,
                Banco = x.Banco,
            })
            .FirstOrDefault();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public ContaBancaria? RecuperarPorId(int id)
    {
        return _context.ContasBancarias
            .Include(b => b.Banco)
            .Include(u => u.Usuario)
            .Where(u => u.IdContaBancaria == id)
            .Select(x => new ContaBancaria
            {
                IdContaBancaria = x.IdContaBancaria,
                Conta = x.Conta,
                ValorDisponivel = x.ValorDisponivel,
                Limite = x.Limite,
                OpenBanking = x.OpenBanking,
                Usuario = x.Usuario,
                Banco = x.Banco,
            })
            .FirstOrDefault();
    }

}