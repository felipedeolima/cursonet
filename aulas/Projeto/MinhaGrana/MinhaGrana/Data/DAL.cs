﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MinhaGrana.Models;

namespace MinhaGrana.Data;
 
/// <summary>
/// 
/// </summary>
/// <typeparam name="T"></typeparam>
public class DAL<T> where T : class
{

    private readonly ApplicationDbContext context;
    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    public DAL(ApplicationDbContext context)
    {
        this.context = context;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IEnumerable<T> Listar()
    {
        return context.Set<T>().ToList();
    }
    /// <summary>
    /// /
    /// </summary>
    /// <param name="objeto"></param>
    public void Adicionar(T objeto)
    {
        context.Set<T>().Add(objeto);

        context.SaveChanges();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="objeto"></param>
    public void Atualizar(T objeto)
    {
        context.Set<T>().Update(objeto);
        context.SaveChanges();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="objeto"></param>
    public void Deletar(T objeto)
    {
        context.Set<T>().Remove(objeto);
        context.SaveChanges();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="condicao"></param>
    /// <returns></returns>
    public T? RecuperarPor(Func<T, bool> condicao)
    {
        return context.Set<T>().FirstOrDefault(condicao);
    }
}

