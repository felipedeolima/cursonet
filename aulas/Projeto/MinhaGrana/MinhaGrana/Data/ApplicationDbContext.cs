using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MinhaGrana.Models;

namespace MinhaGrana.Data;

/// <summary>
/// 
/// </summary>
public class ApplicationDbContext : IdentityDbContext
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="options"></param>
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
	{

	}
    /// <summary>
    /// 
    /// </summary>
    public ApplicationDbContext()
    {
    }
    /// <summary>
    /// 
    /// </summary>
    public DbSet<Categoria> Categorias => Set<Categoria>();
    /// <summary>
    /// 
    /// </summary>
    public DbSet<Banco> Bancos => Set<Banco>();
    /// <summary>
    /// 
    /// </summary>
    public DbSet<Transacao> Transacoes => Set<Transacao>();
    /// <summary>
    /// 
    /// </summary>
    public DbSet<ContaBancaria> ContasBancarias => Set<ContaBancaria>();
}