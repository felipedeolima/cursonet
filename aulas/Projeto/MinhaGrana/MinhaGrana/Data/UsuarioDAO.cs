﻿using AutoMapper;
using iText.Commons.Actions.Contexts;
using iText.Commons.Actions.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MinhaGrana.Data.DTO;
using MinhaGrana.Models;

namespace MinhaGrana.Data;
/// <summary>
/// 
/// </summary>
public class UsuarioDAO
{

    private readonly ApplicationDbContext _context;
    private readonly IMapper _mapper;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="mapper"></param>
    public UsuarioDAO(ApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IEnumerable<UsuarioDTO> Listar()
    {
        return _context.Users
            .Select(x => new UsuarioDTO
            {
                Id = x.Id,
                Nome = x.UserName
            })
            .ToList();
    }

    ///// <summary>
    ///// 
    ///// </summary>
    ///// <param name="id"></param>
    ///// <returns></returns>
    //public UsuarioDTO Get(string id)
    //{
    //    var user = _context.Users
    //        .Where(u => u.Id == id)
    //        .Select(x => new UsuarioDTO
    //        {
    //            Id = x.Id,
    //            Nome = x.UserName
    //        })
    //        .FirstOrDefault();
    //    return user;
    //}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="objeto"></param>
    public void Adicionar(ContaBancariaDAO objeto)
    {
        _context.Set<ContaBancariaDAO>().Add(objeto);

        _context.SaveChanges();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="objeto"></param>
    public void Atualizar(ContaBancariaDAO objeto)
    {
        _context.Set<ContaBancariaDAO>().Update(objeto);
        _context.SaveChanges();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="objeto"></param>
    public void Deletar(ContaBancariaDAO objeto)
    {
        _context.Set<ContaBancariaDAO>().Remove(objeto);
        _context.SaveChanges();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="condicao"></param>
    /// <returns></returns>
    public ContaBancariaDAO? RecuperarPor(Func<ContaBancariaDAO, bool> condicao)
    {
        return _context.Set<ContaBancariaDAO>().FirstOrDefault(condicao);
    }
}

