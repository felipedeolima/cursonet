﻿using iText.Html2pdf;
using iText.Kernel.Pdf;
using MinhaGrana.Data;
using MinhaGrana.Data.DTO;
using MinhaGrana.Models;
using System.Text;

namespace MinhaGrana.Core
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RelatorioService<T> where T : class
    {
        /// <summary>
        /// 
        /// </summary>
        public String TEMPLATE_LISTA_CODIGO_NOME = "<tr>\r\n                    <td>{CODIGO}</td>\r\n                    <td>{NOME}</td>\r\n                </tr>";
        ///// <summary>
        ///// 
        ///// </summary>
        //public String TEMPLATE_CABECALHO_CONTA_BANCARIA = "<tr>\r\n <td>{CONTA}</td> \r\n <td>{VALOR}</td> \r\n <td>{LIMITE}</td> \r\n </tr>";
        ///// <summary>
        ///// 
        ///// </summary>
        //public String TEMPLATE_LISTA_TRANSACOES = "<tr>\r\n <td>{DATA}</td> \r\n <td>{NOME}</td> \r\n <td>{VALOR}</td> \r\n <td>{TIPO}</td> \r\n <td>{CATEGORIA}</td> \r\n </tr>";

        //private readonly TransacaoDAO? _transacaoDAO;

        ///// <summary>
        ///// 
        ///// </summary>
        //public RelatorioService(TransacaoDAO transacaoDAO)
        //{
        //    _transacaoDAO = transacaoDAO;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        //public RelatorioService()
        //{
        //    _transacaoDAO = null;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dal"></param>
        /// <returns></returns>
        public async Task<byte[]> CriarRelatorioAsync(DAL<T> dal)
        {
            string tipo = string.Empty;
            using var pdfMemoryStream = new MemoryStream();
            PdfWriter pdfWriter = new(pdfMemoryStream);
            PdfDocument pdfDocument = new(pdfWriter);

            tipo = typeof(T).Name.ToString();

            var html = await RelatorioService<T>.ObterHtmlRelatorioAsync(tipo);

            html = SubstituirTagsHtml(dal, html);

            var byteArrayHtml = Encoding.UTF8.GetBytes(html);

            using (var htmlMemoryStream = new MemoryStream(byteArrayHtml))
            {
                HtmlConverter.ConvertToPdf(htmlMemoryStream, pdfDocument);
            }

            pdfDocument.Close();
            var pdfStream = pdfMemoryStream.ToArray();

            return pdfStream;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="conta"></param>
        ///// <returns></returns>
        //public async Task<byte[]> CriarRelatorioAsyncTransacao(string conta)
        //{
        //    string tipo = string.Empty;
        //    using var pdfMemoryStream = new MemoryStream();
        //    PdfWriter pdfWriter = new(pdfMemoryStream);
        //    PdfDocument pdfDocument = new(pdfWriter);

        //    tipo = typeof(T).Name.ToString();

        //    var html = await RelatorioService<T>.ObterHtmlRelatorioAsync(tipo);

        //    html = SubstituirTagsHtmlTransacao(conta, html);

        //    var byteArrayHtml = Encoding.UTF8.GetBytes(html);

        //    using (var htmlMemoryStream = new MemoryStream(byteArrayHtml))
        //    {
        //        HtmlConverter.ConvertToPdf(htmlMemoryStream, pdfDocument);
        //    }

        //    pdfDocument.Close();
        //    var pdfStream = pdfMemoryStream.ToArray();

        //    return pdfStream;
        //}

        private string SubstituirTagsHtml(DAL<T> dal, string html)
        {
            StringBuilder htmlBuilder = new(html);

            var lista = dal.Listar();
            String template = "";
            foreach (T obj in lista)
            {
                switch(obj.GetType().Name)
                {
                    case "Banco":
                        Banco? banco = obj as Banco;
                        if (banco is null) break;
                        template += TEMPLATE_LISTA_CODIGO_NOME.Replace("{CODIGO}", banco.IdBanco.ToString()).Replace("{NOME}", banco.Nome);
                        break;

                    case "Categoria":
                        Categoria? categoria = obj as Categoria;
                        if (categoria is null) break;
                        template += TEMPLATE_LISTA_CODIGO_NOME.Replace("{CODIGO}", categoria.IdCategoria.ToString()).Replace("{NOME}", categoria.Nome);
                        break;
                }
            }
            
            Replace(htmlBuilder, "{{LISTA_INSTITUICOES_FINANCEIRAS}}", template);

            return htmlBuilder.ToString();
        }

        //private string SubstituirTagsHtmlTransacao(string conta, string html)
        //{
        //    StringBuilder htmlBuilder = new(html);

        //    //var lista = dal.Listar();


        //    //TODO
        //    //CABECALHO_CONTA_BANCARIA

        //    //TransacaoDAO? transacaoDAO = new TransacaoDAO();

        //    var lista = transacaoDAO!.ListarPorConta(conta);
        //    String template = "";
        //    foreach (TransacaoDTO t in lista)
        //    {
        //        if (t is null) break;
        //        template += TEMPLATE_LISTA_TRANSACOES
        //            .Replace("{DATA}", t.Data.ToString())
        //            .Replace("{NOME}", t.Nome)
        //            .Replace("{VALOR}", t.Valor.ToString())
        //            .Replace("{TIPO}", t.Tipo.ToString())
        //            .Replace("{CATEGORIA}", t.NomeCategoria);
        //    }

        //    Replace(htmlBuilder, "{{LISTA_TRANSACOES}}", template);

        //    return htmlBuilder.ToString();
        //}

        private static void Replace(StringBuilder builder, string placeholder, string value)
        {
            builder.Replace(placeholder, string.IsNullOrWhiteSpace(value) ? String.Empty : value);
        }

        private static async Task<string> ObterHtmlRelatorioAsync(string tipo)
        {
            var html = string.Empty;
            html = await File.ReadAllTextAsync($"./Core/Relatorio_{tipo}.html");

            return html;
        }
    }
}