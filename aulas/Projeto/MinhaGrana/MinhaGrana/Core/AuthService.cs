﻿using Microsoft.AspNetCore.Identity;
using MinhaGrana.Data;
using MinhaGrana.Data.DTO;

namespace MinhaGrana.Core;
/// <summary>
/// 
/// </summary>
public class AuthService
{
    private readonly UsuarioDAO _usuarioDAO;
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="usuarioDAO"></param>
    public AuthService(UsuarioDAO usuarioDAO)
    {
        _usuarioDAO = usuarioDAO;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IEnumerable<UsuarioDTO> Listar()
    {
        return _usuarioDAO.Listar();
    }
}
