﻿using MinhaGrana.Data;
using MinhaGrana.Data.DTO;
using MinhaGrana.Models;

namespace MinhaGrana.Core;

/// <summary>
/// 
/// </summary>
public class ContaBancariaService
{
    /// <summary>
    /// 
    /// </summary>
    private ContaBancariaDAO _contaBancariaDAO;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="contaBancariaDAO"></param>
    public ContaBancariaService(ContaBancariaDAO contaBancariaDAO)
    {
        _contaBancariaDAO = contaBancariaDAO;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IEnumerable<ContaBancariaDTO> Listar()
    {
        return _contaBancariaDAO.Listar();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IEnumerable<ContaBancariaDTO> Listar(string user)
    {
        return _contaBancariaDAO.ListarContaPorUsuario(user);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="conta"></param>
    /// <returns></returns>
    public ContaBancaria RecuperarPorConta(string conta)
    {
        var contaRecuperada = _contaBancariaDAO.RecuperarPorConta(conta);
        if(contaRecuperada == null) { return new ContaBancaria(); }
        return contaRecuperada;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public ContaBancariaDTO RecuperarPorId(int id)
    {
        var conta = _contaBancariaDAO.RecuperarPorId(id);
        var contaRecuperada = new ContaBancariaDTO();
        if (conta == null) { return contaRecuperada; }
        contaRecuperada.NomeBanco = conta.Banco!.Nome;
        contaRecuperada.NomeUsuario = conta.Usuario!.Email;
        contaRecuperada.OpenBanking = conta.OpenBanking;
        contaRecuperada.IdContaBancaria = conta.IdContaBancaria;
        contaRecuperada.Conta = conta.Conta;
        contaRecuperada.ValorDisponivel = conta.ValorDisponivel;
        contaRecuperada.Limite = conta.Limite;

        return contaRecuperada;
    }

}
