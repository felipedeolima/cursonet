﻿using AutoMapper;
using iText.Html2pdf;
using iText.Kernel.Pdf;
using MinhaGrana.Data;
using MinhaGrana.Data.DTO;
using MinhaGrana.Models;
using System.Text;

namespace MinhaGrana.Core;

/// <summary>
/// Serviço da transação
/// </summary>
public class TransacaoService
{
    /// <summary>
    /// Template com cabeçalho da conta bancária
    /// </summary>
    public String TEMPLATE_CABECALHO_CONTA_BANCARIA = "<h4>Conta Bancária: {CONTA} ({NOMECLIENTE})</h4> <h4>Valor: R$ {VALOR}</h4> <h4>Limite: R$ {LIMITE}</h4>";

    /// <summary>
    /// Template com a lista de transações
    /// </summary>
    public String TEMPLATE_LISTA_TRANSACOES = "<tr>\r\n <td>{DATA}</td> \r\n <td>{NOME}</td> \r\n <td>R$ {VALOR}</td> \r\n <td>{TIPO}</td> \r\n <td>{CATEGORIA}</td> \r\n </tr>";
    
    /// <summary>
    /// DAO para acesso aos dados do modelo da transação
    /// </summary>
    private TransacaoDAO _transacaoDAO;

    private CategoriaDAO _categoriaDAO;
    private ContaBancariaDAO _contaBancariaDAO;
    private readonly IMapper _mapper;


    /// <summary>
    /// Construtor do serviço, passando DAO de transação, categoria e conta bancária, além de mapeamento DTO - DAO
    /// </summary>
    /// <param name="transacaoDAO"></param>
    /// <param name="categoriaDAO"></param>
    /// <param name="contaBancariaDAO"></param>
    /// <param name="mapper"></param>
    public TransacaoService(TransacaoDAO transacaoDAO, CategoriaDAO categoriaDAO, ContaBancariaDAO contaBancariaDAO, IMapper mapper)
    {
        _transacaoDAO = transacaoDAO;
        _categoriaDAO = categoriaDAO;
        _contaBancariaDAO = contaBancariaDAO;
        _mapper = mapper;
    }

    /// <summary>
    /// Listagem de todas as transações
    /// </summary>
    /// <returns>TransacaoDTO com dados de nome de categoria e conta bancária</returns>
    public IEnumerable<TransacaoDTO> Listar()
    {
        return _transacaoDAO.Listar();
    }

    /// <summary>
    /// Listagem de transações por conta bancária
    /// </summary>
    /// <returns>TransacaoDTO com dados de nome de categoria e conta bancária</returns>
    public IEnumerable<TransacaoDTO> Listar(string conta)
    {
        return _transacaoDAO.ListarPorConta(conta);
    }

    /// <summary>
    /// Listagem de transações por nome da transação
    /// </summary>
    /// <returns>TransacaoDTO com dados de nome de categoria e conta bancária</returns>
    public IEnumerable<TransacaoDTO> ListarPorNome(string nomeTransacao)
    {
        return _transacaoDAO.ListarPorNome(nomeTransacao);
    }

    /// <summary>
    /// Obter template do relatório em PDF das transações
    /// </summary>
    /// <param name="tipo"></param>
    /// <returns></returns>
    private static async Task<string> ObterHtmlRelatorioAsync(string tipo)
    {
        var html = string.Empty;
        html = await File.ReadAllTextAsync($"./Core/Relatorio_{tipo}.html");

        return html;
    }

    /// <summary>
    /// Criação do relatório da transação
    /// </summary>
    /// <param name="conta">Relatório pela conta bancária</param>
    /// <returns></returns>
    public async Task<byte[]> CriarRelatorioAsyncTransacao(string conta)
    {
        string tipo = string.Empty;
        using var pdfMemoryStream = new MemoryStream();
        PdfWriter pdfWriter = new(pdfMemoryStream);
        PdfDocument pdfDocument = new(pdfWriter);

        var html = await ObterHtmlRelatorioAsync("Transacao");

        html = SubstituirTagsHtmlTransacao(conta, html);

        var byteArrayHtml = Encoding.UTF8.GetBytes(html);

        using (var htmlMemoryStream = new MemoryStream(byteArrayHtml))
        {
            HtmlConverter.ConvertToPdf(htmlMemoryStream, pdfDocument);
        }

        pdfDocument.Close();
        var pdfStream = pdfMemoryStream.ToArray();

        return pdfStream;
    }
    /// <summary>
    /// Método de substituição de valores
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="placeholder"></param>
    /// <param name="value"></param>
    private static void Replace(StringBuilder builder, string placeholder, string value)
    {
        builder.Replace(placeholder, string.IsNullOrWhiteSpace(value) ? String.Empty : value);
    }

    private string SubstituirTagsHtmlTransacao(string conta, string html)
    {
        StringBuilder htmlBuilder = new(html);

        var contaBancaria = _transacaoDAO!.RecuperarContaBancaria(conta);
        String cabecalho = TEMPLATE_CABECALHO_CONTA_BANCARIA;
        cabecalho = cabecalho
            .Replace("{CONTA}", contaBancaria.Conta)
            .Replace("{NOMECLIENTE}", contaBancaria.NomeUsuario)
            .Replace("{VALOR}", contaBancaria.ValorDisponivel.ToString())
            .Replace("{LIMITE}", contaBancaria.Limite.ToString());

        var lista = _transacaoDAO!.ListarPorConta(conta);
        String template = "";
        foreach (TransacaoDTO t in lista)
        {
            if (t is null) break;
            template += TEMPLATE_LISTA_TRANSACOES
                .Replace("{DATA}", t.Data.ToString())
                .Replace("{NOME}", t.Nome)
                .Replace("{VALOR}", t.Valor.ToString())
                .Replace("{TIPO}", t.Tipo=='C' ? "Crédito" : "Débito")
                .Replace("{CATEGORIA}", t.NomeCategoria);
        }
        
        Replace(htmlBuilder, "{{CABECALHO_CONTA_BANCARIA}}", cabecalho);

        Replace(htmlBuilder, "{{LISTA_TRANSACOES}}", template);

        return htmlBuilder.ToString();
    }

    /// <summary>
    /// Adicionar transação do cliente
    /// </summary>
    /// <param name="transacaoDTO"></param>
    /// <returns>True se deu certo, false se não encontrou ou erro</returns>
    public (Boolean, Decimal) Adicionar(TransacaoDTO transacaoDTO) {
        Transacao transacao = new();

        transacao.Nome = transacaoDTO.Nome;
        transacao.Valor = transacaoDTO.Valor;
        transacao.Tipo = transacaoDTO.Tipo;
        transacao.Data = transacaoDTO.Data;
        transacao.ContaBancaria = _contaBancariaDAO.RecuperarPorConta(transacaoDTO.ContaBancaria!);


        _transacaoDAO.Adicionar(transacao);
        if (transacao.ContaBancaria is not null)
        {
            //Crédito
            if (transacaoDTO.Tipo == 'C')
            {
                transacao.ContaBancaria.ValorDisponivel += transacao.Valor;
            }
            else //Débito
            {
                transacao.ContaBancaria.ValorDisponivel -= transacao.Valor;
            }
            _contaBancariaDAO.Atualizar(transacao.ContaBancaria);
            return (true, transacao.ContaBancaria.ValorDisponivel);
        }
        return (false, transacao.ContaBancaria!.ValorDisponivel);
    }

    /// <summary>
    /// Atualização da transação
    /// </summary>
    /// <param name="transacaoDTO">TransacaoDTO com os dados para atualizar</param>
    /// <returns>True se deu certo, false se não encontrou ou erro</returns>
    public (Boolean,Decimal) Atualizar(TransacaoDTO transacaoDTO)
    {
        Transacao transacao = new();
        transacao = _transacaoDAO.ListarPorNomeUnico(transacaoDTO.Nome)!;
        if (transacao == null) { return (false, 0); }

        if (transacao.ContaBancaria is not null)
        {
            if (transacao.Tipo == 'C')
            {
                transacao.ContaBancaria.ValorDisponivel -= transacao.Valor;
            }
            else
            {
                transacao.ContaBancaria.ValorDisponivel += transacao.Valor;
            }
        }        

        //Atualizando os dados
        transacao.Valor = transacaoDTO.Valor;
        transacao.Tipo = transacaoDTO.Tipo;
        transacao.Data = transacaoDTO.Data;
        
        if (transacao.ContaBancaria is not null)
        {
            //Crédito
            if (transacaoDTO.Tipo == 'C')
            {
                transacao.ContaBancaria.ValorDisponivel += transacao.Valor;                
            }
            else //Débito
            {
                transacao.ContaBancaria.ValorDisponivel -= transacao.Valor;
            }
            _transacaoDAO.Atualizar(transacao);
            return (true, transacao.ContaBancaria.ValorDisponivel);
        }
        else
        {
            return (false, transacao.ContaBancaria!.ValorDisponivel);
        }
    }

    /// <summary>
    /// Exclusão da transação, se listada e encontrada
    /// </summary>
    /// <param name="id">Identificador da transação</param>
    /// <returns>True se exclusão concluída, False se não encontrou</returns>
    public bool Excluir(int id)
    {
        var transacao = _transacaoDAO.ListarPorId(id);
        if (transacao == null) { return false; }
        _transacaoDAO.Deletar(transacao);
        return true;
    }
}
