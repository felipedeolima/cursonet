﻿using MinhaGrana.Data.DTO;
using MinhaGrana.Data;
using MinhaGrana.Models;
using Microsoft.AspNetCore.Identity;

namespace MinhaGrana.Core
{
    /// <summary>
    /// Service de Categoria
    /// </summary>
    public class CategoriaService
    {
        private readonly UserManager<IdentityUser> _userManager;
        private CategoriaDAO _categoriaDAO;
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="categoriaDAO"></param>
        /// <param name="userManager"></param>
        public CategoriaService(CategoriaDAO categoriaDAO, UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
            _categoriaDAO = categoriaDAO;
        }

        /// <summary>
        /// Listar Todas Categorias
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CategoriaDTO> Listar()
        {
            return _categoriaDAO.Listar();
        }

        /// <summary>
        /// Listar categorias de um usuário
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IEnumerable<CategoriaDTO> Listar(string user)
        {
            return _categoriaDAO.ListarCategoriaPorUsuario(user);
        }

        /// <summary>
        /// Adicionar categoria a um usuario
        /// </summary>
        /// <param name="categoria"></param>
        /// <param name="user"></param>
        public void Adicionar(CategoriaDTO categoria, IdentityUser user)
        {

            Categoria categoriaAInserir = new Categoria();
            categoriaAInserir.Nome = categoria.Nome;
            categoriaAInserir.Usuario = user;

            _categoriaDAO.Adicionar(categoriaAInserir);
        }

        /// <summary>
        /// Deletar categoria de um usuário
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool Deletar(int id, IdentityUser user)
        {
            Categoria categoria = _categoriaDAO.RecuperarPorId(id);
            if (categoria is null) return false;
            if (categoria.Usuario is null) return false;
            if (categoria.Usuario is not null && categoria.Usuario != user) return false;

            _categoriaDAO.Deletar(categoria);
            return true;
        }

        /// <summary>
        /// Deletar qualquer categoria
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeletarComoAdmin(int id)
        {
            Categoria categoria = _categoriaDAO.RecuperarPorId(id);
            if (categoria is null) return false;
            
            _categoriaDAO.Deletar(categoria);
            return true;
        }

        /// <summary>
        /// Atualizar categorias de um usuário
        /// </summary>
        /// <param name="categoriaDTO"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool Atualizar(CategoriaDTO categoriaDTO, IdentityUser user)
        {
            Categoria categoria = _categoriaDAO.RecuperarPorId(categoriaDTO.Id);
            if (categoria is null) return false;
            if (categoria.Usuario is not null && categoria.Usuario != user) return false;

            categoria.Nome = categoriaDTO.Nome;

            _categoriaDAO.Atualizar(categoria);
            return true;
        }

        /// <summary>
        /// Atualizar categorias como Admin
        /// </summary>
        /// <param name="categoriaDTO"></param>
        /// <returns></returns>
        public bool AtualizarComoAdmin(CategoriaDTO categoriaDTO)
        {
            Categoria categoria = _categoriaDAO.RecuperarPorId(categoriaDTO.Id);
            if (categoria is null) return false;

            categoria.Nome = categoriaDTO.Nome;

            _categoriaDAO.Atualizar(categoria);
            return true;
        }

        /// <summary>
        /// Recuperar categoria por ID
        /// </summary>
        /// <param name="IdCategoria"></param>
        /// <returns></returns>
        public Categoria RecuperarPorId(int IdCategoria)
        {
            return _categoriaDAO.RecuperarPorId(IdCategoria);
        }
    }
}
