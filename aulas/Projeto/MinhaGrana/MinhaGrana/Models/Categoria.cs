﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace MinhaGrana.Models;
/// <summary>
/// 
/// </summary>
public class Categoria
{
    /// <summary>
    /// 
    /// </summary>
    [Key]
    public int IdCategoria { get; set; }
    /// <summary>
    /// 
    /// </summary>
    [Required]
    [StringLength(200)]
    public string Nome { get; set; } = string.Empty;

    /// <summary>
    /// 
    /// </summary>
    [ForeignKey("IdUsuario")]
    public IdentityUser? Usuario { get; set; }
}
