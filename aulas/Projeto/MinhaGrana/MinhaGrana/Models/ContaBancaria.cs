﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace MinhaGrana.Models;

/// <summary>
/// 
/// </summary>
public class ContaBancaria
{
    /// <summary>
    /// 
    /// </summary>
    [Key]
    public int IdContaBancaria { get; set; }
    /// <summary>
    /// 
    /// </summary>
    [StringLength(200)]
    public string Conta { get; set; } = string.Empty;
    /// <summary>
    /// 
    /// </summary>
    [Precision(17, 2)]
    public decimal ValorDisponivel { get; set; }
    /// <summary>
    /// 
    /// </summary>
    [Precision(13,2)]
    public decimal Limite { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public bool OpenBanking { get; set; }
    /// <summary>
    /// 
    /// </summary>
    [ForeignKey("Id")]
    public IdentityUser? Usuario { get; set; }
    /// <summary>
    /// 
    /// </summary>
    [ForeignKey("IdBanco")]
    public Banco? Banco { get;set; }
}
