﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MinhaGrana.Models;
/// <summary>
/// 
/// </summary>
public class Banco
{
    /// <summary>
    /// 
    /// </summary>
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int IdBanco { get; set; }
    /// <summary>
    /// 
    /// </summary>
    [Required]
    [StringLength(200)]
    public string Nome { get; set; } = string.Empty;
}
