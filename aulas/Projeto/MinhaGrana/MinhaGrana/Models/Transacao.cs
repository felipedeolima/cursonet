﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MinhaGrana.Models;

/// <summary>
/// Modelagem da Transacao
/// </summary>
public class Transacao
{
    /// <summary>
    /// Identificador
    /// </summary>
    [Key]
    public int IdTransacao { get; set; }
    /// <summary>
    /// Nome, obrigatório, comprimento máximo 200 caracteres
    /// </summary>
    [Required]
    [StringLength(200)]
    public string Nome { get; set; } = string.Empty;
    /// <summary>
    /// Valor, 10 casas decimais e precisão de 2
    /// </summary>
    [Required]
    [Precision(10,2)]
    public decimal Valor { get; set; }
    /// <summary>
    /// Tipo D (Débito) ou C (Crédito)
    /// </summary>
    [Required]
    public char Tipo { get; set; }

    /// <summary>
    /// Data com hora
    /// </summary>
    [Required]
    public DateTime Data { get; set; }
    /// <summary>
    /// Identificador da categoria, obrigatório
    /// </summary>
    [ForeignKey("IdCategoria")]
    public Categoria? Categoria { get; set; }
    /// <summary>
    /// Identificador da conta bancária
    /// </summary>
    [ForeignKey("IdContaBancaria")]
    public ContaBancaria? ContaBancaria { get; set; }

}
