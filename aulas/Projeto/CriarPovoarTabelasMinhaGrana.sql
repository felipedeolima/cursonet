use trilha;

if exists (select 1
          from sysobjects
          where name = 'ContaBancaria')
   drop table ContaBancaria
go

if exists (select 1
          from sysobjects
          where name = 'Transacao')
   drop table Transacao
go

if exists (select 1
          from sysobjects
          where name = 'Banco')
   drop table Banco
go

if exists (select 1
          from sysobjects
          where name = 'Usuario')
   drop table Usuario
go

if exists (select 1
          from sysobjects
          where name = 'Categoria')
   drop table Categoria
go

CREATE TABLE Categoria (
    Id INT PRIMARY KEY,
    Nome VARCHAR(255)
);

CREATE TABLE Banco (
    Id INT PRIMARY KEY,
    Nome VARCHAR(255)
);

CREATE TABLE Usuario (
    Id INT PRIMARY KEY,
    Nome VARCHAR(255)
);
 
CREATE TABLE ContaBancaria (
    Id INT PRIMARY KEY,
    Conta VARCHAR(255),
	Limite DECIMAL(17,2),
	OpenBanking BIT,
    IdUsuario INT NOT NULL,
	IdBanco INT NOT NULL,
    FOREIGN KEY (IdUsuario) REFERENCES Usuario(Id),
	FOREIGN KEY (IdBanco) REFERENCES Banco(Id)
);

CREATE TABLE Transacao (
    Id INT PRIMARY KEY,
    Nome VARCHAR(255),
	Valor DECIMAL(13,2),
	Tipo CHAR(1),
	IdCategoria INT NOT NULL,
	IdContaBancaria INT NOT NULL,
    FOREIGN KEY (IdCategoria) REFERENCES Categoria(Id),
	FOREIGN KEY (IdContaBancaria) REFERENCES Banco(Id),
);

INSERT INTO Usuario VALUES (1,'FELIPE de Oliveira Lima');
INSERT INTO Usuario VALUES (2,'ISMAEL Bento Costa');
INSERT INTO Usuario VALUES (3,'JOALITON Luan Pereira Ferreira');
INSERT INTO Usuario VALUES (4,'MAXWELL Wendell Bezerra Rocha');

INSERT INTO Categoria VALUES (1,'Alimenta��o');
INSERT INTO Categoria VALUES (2,'Sa�de');
INSERT INTO Categoria VALUES (3,'Entretenimento');
INSERT INTO Categoria VALUES (4,'Estudos');
INSERT INTO Categoria VALUES (5,'Moradia');
INSERT INTO Categoria VALUES (6,'Transporte');
INSERT INTO Categoria VALUES (7,'Viagens');
INSERT INTO Categoria VALUES (8,'Rendimentos');
INSERT INTO Categoria VALUES (9,'Ganhos');

INSERT INTO Banco VALUES (1,'Banco do Nordeste');
INSERT INTO Banco VALUES (2,'Banco do Brasil');
INSERT INTO Banco VALUES (3,'Caixa Econ�mica Federal');
INSERT INTO Banco VALUES (4,'Nubank');
INSERT INTO Banco VALUES (5,'C6');

INSERT INTO ContaBancaria VALUES (1,'12345-6',10000,0,3,1);
INSERT INTO ContaBancaria VALUES (2,'11235-6',10,0,2,2);
INSERT INTO ContaBancaria VALUES (3,'125145-6',1230,0,4,3);
INSERT INTO ContaBancaria VALUES (4,'1236265-6',200,0,1,3);
INSERT INTO ContaBancaria VALUES (5,'1262465-6',50000,0,3,5);


--Categoria //  Conta Banc�ria
INSERT INTO Transacao VALUES (1,'Almo�o',20,'D',1,3)
INSERT INTO Transacao VALUES (2,'Rem�dio pra verme',30,'D',1,1)
INSERT INTO Transacao VALUES (3,'Financiamento',1600,'D',5,4)
INSERT INTO Transacao VALUES (4,'Aluguel',1400,'C',1,2)
INSERT INTO Transacao VALUES (5,'Livros para Concurso',100,'D',4,5)
INSERT INTO Transacao VALUES (6,'Casa de Praia no Anivers�rio - 3 di�rias',1200,'D',1,4)
INSERT INTO Transacao VALUES (7,'Sal�rio Fevereiro - 24',7999,'C',9,2)
INSERT INTO Transacao VALUES (8,'Balada na Toca da Coruja',200,'D',3,5)
INSERT INTO Transacao VALUES (9,'Quadra na Loteria',10500,'D',8,5)
INSERT INTO Transacao VALUES (10,'Gasolina no Fusquinha',250,'D',6,1)

select * from Usuario;
select * from Categoria;
select * from Banco;
select * from ContaBancaria;
select * from Transacao;