## Modelo Entidade Relacionamento MinhaGrana 💰

```mermaid
erDiagram
    Usuario ||--o{ Transacao : "registra"
    Transacao }|..|{ Categoria : "categorizada como"
    Transacao }|..|| ContaBancaria : "associada com"
    Usuario }|..|| ContaBancaria : "possui"
    Transacao }|..|| CategoriaTransacao : "pertence a"
```

Script para criação e povoamento da base: [SQL](https://gitlab.com/felipedeolima/cursonet/-/blob/main/aulas/Projeto/CriarPovoarTabelasMinhaGrana.sql)

Neste diagrama:

Usuário: Armazena informações sobre os usuários, como nome de usuário, e-mail etc.
Transação: Registra detalhes de despesas e receitas, incluindo o valor, data e descrição.
Categoria: Define as categorias para classificar as transações (por exemplo, alimentação, transporte, moradia).
Conta Bancária: Armazena informações sobre as contas bancárias dos usuários.
Relação Usuário: Associa usuários a suas transações.
Relação Transação: Associa transações a categorias.
Relação Usuário com Conta Bancária: Associa usuários a suas contas bancárias.
Lembre-se de que este é um modelo conceitual e pode ser adaptado conforme necessário durante o desenvolvimento do projeto. Certifique-se de envolver os usuários e especialistas em finanças para validar e ajustar o modelo de acordo com os requisitos específicos. 


