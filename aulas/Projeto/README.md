# Projeto MinhaGrana

Bem-vindo ao nosso projeto MinhaGrana do grupo 4 da trilha FIAP! Aqui você encontrará toda a nossa referência e o Todo List para a entrega desse projeto orquestrado pelo professor Emerson Delatorre.

# Integrantes do Grupo 4 - Onboarding .NET

- FELIPE de Oliveira Lima
- ISMAEL Bento Costa
- JOALITON Luan Pereira Ferreira
- MAXWELL Wendell Bezerra Rocha
- ELOFRAN Marques Silva
- Fernando Moreira de Souza

## Programação do Projeto 📅

### ✅ Introdução e Setup do Ambiente

- Conhecimento a expertise de cada um. Setup do ambiente de desenvolvimento com Visual Studio 2022, .NET Core 6 e C# para todos os integrantes.

### ✅ Análise do Projeto

- Revisão dos passos necessários e coletando informações sobre o objetivo do projeto. Discussão sobre quais etapas devemos detalhar nosso projeto, ajustando ao cronograma passado pelo orientador do aprendizado que se inicia a partir da apresentação deste projeto. Montagem do README.md.

    [Projeto](https://delatorre.notion.site/O-Projeto-94094bed1142420a88f196f319614c1b)

### ✅ Requisitos
- Iniciamos o levantamento dos requisitos iniciais (em desenvolvimento)
    [Requisitos](./Requisitos.md)

### ✅ Modelo Entidade-Relacionamento

- Esboço do modelo de dados indicado e refinamento, coletando sugestões do time sobre outras necessidades.

    [Modelo](./Modelo.md)

### Iniciando a Montagem do Banco de Dados via Context / Models
- Concluído

### Povoando o banco de dados e criando cenários para os futuros testes 
- Concluído

### Iniciando a construção da API (Controllers / Models / Context)
- Revisão de aplicação de boas práticas de programação e design de API
- Concluído

### Estruturando as primeiras consultas / Swagger
- Concluído

### Gerenciando o CRUD necessário
- Revisão de aplicação de boas práticas de programação e design de API

### Aplicando conceitos de segurança
- Concluído

### Integrações com outra API
- Revisão
- Em construção

### Integrações com outra API
- Revisão de aplicação de boas práticas de programação e design de API
- Em construção

### Iniciando QA
- Em construção

### Execução de Testes
- Em construção
- Revisão

### Revisando Processo
- Em construção